\documentclass[a4paper,12pt,oneside]{book}

\pagestyle{plain}

\usepackage[T1]{fontenc} % imposta la codifica dei font
\usepackage[utf8]{inputenc} % lettere accentate da tastiera
\usepackage[english]{babel} % per scrivere in italiano
\usepackage{pgfplots}
\usepackage{xcolor}
\definecolor{light-gray}{gray}{0.95}

\usepackage{listings}
\lstset{%
frame=trBL
tabsize=2,%
xleftmargin=-25pt,%
xrightmargin=-28pt,%
language=Java,%
basicstyle=\small\ttfamily,%
keywordstyle=\color{black}\bfseries,%
commentstyle=\color{gray},%
stringstyle=\color{black},%
showstringspaces=false,%
breaklines=true,%
backgroundcolor=%
\color{light-gray}}

\usepackage{amsmath} % per scrivere le formule matematiche
\usepackage{array}
\usepackage{tikz}
\usepackage{color}
\usepackage{calc}
\usepackage{booktabs}
\usepackage{relsize}
\usepackage{siunitx}
\sisetup{
  fixdp,dp=3,
  load=derived,
  unitsep=thin,
  valuesep=thin,
  decimalsymbol=comma,
  % digitsep=thin,
  % group-separator={},% ver. 2
  % sepfour=false,
  % round-mode = places,
  % round-precision = 3
}

\usepackage{graphicx}
\graphicspath{{../../images/}}
\makeatletter
\setlength{\@fptop}{0pt}
\makeatother

\usepackage{adjustbox}
\usepackage{rotating}
\usepackage{tikz}
\usepackage{microtype}
\usepackage[hidelinks]{hyperref}

\begin{document}
\chapter{The Payload-Range class}
The Payload-Range diagram is a very important resource in aircrafts design because it allows to illustrate an aircraft operational limits by showing the trade-off relationship between the payload that can be carried and the range that can be flown; moreover, in synergy with the D.O.C. diagram, it’s very useful to the future aircraft customers to have an idea of expected profits derived by their investment.

Payload-Range is also a key feature of the design requirements so it’s possible to state that its relevance is felt through all the preliminary and conceptual design phases.

\section{Theoretical background}
To better understand the theoretical background behind this diagram, the first step is to focus the attention upon the link between the range and the fuel consumption, which is influenced by aircraft weight through Breguet formulas. 

It’s possible to imagine, at first, an aircraft in which payload and fuel weights can be managed at will, so that, in this completely flexible aircraft, the more the required range is, the less is the payload in order to carry more fuel at a specified maximum take-off weight; this leads to the diagram in figure~\ref{fig:Figure1}.

\begin{figure}[ht]
\centering
\includegraphics[keepaspectratio, width=0.6\textwidth]{Flexible_Payload_Range}
\caption{Payload-Range for a completely flexible aircraft}
\label{fig:Figure1}
\end{figure}

Because the payload is carried in the fuselage while the fuel is carried in wing tanks, the more the payload, the more the weight in the fuselage; this means that wings are incrementally more bending loaded so that a heavier structure is required. However, in this way the operating empty weight grows up limiting the payload the aircraft can carry, at that maximum take-off and fuel weights, for that range; the consequence of this is that, in particular for long range missions, the payload is too low, providing low profits.

In order to avoid this situation it’s possible to decide, preventively, the maximum payload weight the aircraft can carry in fuselage so that the maximum bending load for wings and, in consequence their structural weight, is set; this particular weight is named maximum zero fuel weight (MZFW).

As a result of this, the previous diagram changes in the one reported in Figure~\ref{fig:Figure2} which starts with the maximum zero fuel weight and keep it constant until the maximum take-off weight is reached. From this point on, the far the aircraft have to go, the more the fuel it needs, but, since the max take-off weight is set, the only way to increase fuel weight is to reduce the payload; this condition is represented by the second segment of the diagram and, along this, the aircraft weight is always equal to the maximum take-off weight. With further increase of the required range for the aircraft, the fuel tanks maximum capacity will be reached prevent to store more fuel; the only solution at this problem is to put the additional fuel into the fuselage, reducing, consequently, the payload weight in order to respect the maximum zero fuel weight limitation. Along this last segment the payload weight decreases more rapidly with increasing range reaching, ideally, the value of zero at which the flight mission is useless; moreover the aircraft weight is not equal to the maximum take-off weight anymore but to a lower one.

\bigskip
Now that the theory behind the Payload-Range is explained, it’s interesting to see how to build up the diagram. For this purpose at least the preliminary weight estimation has to be done in order to know the value of the following weights.

\begin{figure}[ht]
\centering
\includegraphics[keepaspectratio, width=0.6\textwidth]{Payload_Range}
\caption{Payload-Range with MZFW limitation}
\label{fig:Figure2}
\end{figure}

\begin{itemize}
\item W\textsubscript{TO}
\item W\textsubscript{OE}
\item W\textsubscript{Payload}
\end{itemize}

Furthermore four fundamental couples of payload and range values have to be defined:

\begin{itemize}
\item Point A, in which the payload is the maximum allowed and the range is zero.
\item Point B, in which the maximum range, with maximum take-off weight and maximum payload, is reached. This range value is called Armoinc Range.
\item Point C, in which the fuel tanks full capacity is reached with a payload lower than the maximum one and still the maximum take-off weight.
\item Point D, in which the take-off weight decreases because of the reduction of payload to zero in order to store more fuel in the fuselage.
\end{itemize}

Since point A is very simple to obtain, it’s more interesting to analyze how to calculate points B, C and D coordinates.

For point B it’s necessary to focus the attention upon Breguet formulas in order to calculate the maximum range the aircraft can fly at maximum take-off weight with maximum payload; these are different for jet or propeller aircraft and can be written as follow, with R in $\SI{}{\kilogram}$, SFC in $\frac{lb}{hp\cdot\hour}$ and SFCJ in $\frac{lb}{lb\cdot\hour}$.

\begin{eqnarray}
R=603.5\cdot \left(\frac{\eta_{p}}{SFC}\right)_{cruise}\cdot \left(\frac{L}{D}\right)_{cruise}\cdot \ln\left(\frac{W_{i}}{W_{f}}\right) %
\qquad \textrm {\parbox[t][][t]{.25\linewidth } {\raggedright propeller aircraft}}
\label{eqn:Equation1.1}
\end{eqnarray}

\begin{eqnarray}
R=\left(\frac{V}{SFCJ}\right)_{cruise}\cdot \left(\frac{L}{D}\right)_{cruise}\cdot \ln\left(\frac{W_{i}}{W_{f}}\right) %
\qquad \textrm {\parbox[t][][t]{.25\linewidth} {jet aircraft}}
\label{eqn:Equation1.2}
\end{eqnarray}

\bigskip
Knowing the specific fuel consumption, the aerodynamic and propeller efficiency, or speed for jet aircraft, in cruise condition, the only unknown left for calculate the range is the weight ratio. In order to calculate this it’s necessary to start by evaluating the amount of fuel the airplane can take on board, at maximum take-off weight and maximum payload, with the following formula.

\begin{equation}
W_{TO,max}=W_{OE}+W_{payoad,max}+W_{fuel}
\label{eqn:Equation1.3}
\end{equation}

\bigskip
Once the used fuel weight is known, it’s possible to use the fuel fraction method from the weight estimation design phase in order to find the total weight ratio across the entire aircraft mission. 

\begin{equation}
W_{fuel}=\left(1-M_{ff}\right)\cdot W_{TO}
\label{eqn:Equation1.4}
\end{equation}

\bigskip
$M_{ff}$ is the fuel fraction of the entire mission profile given by the following expression.

\begin{equation}
M_{ff}=\frac{W_1}{W_{TO}}\cdot 
	   \frac{W_2}{W_{1}}\cdot
	   \frac{W_3}{W_{2}}\cdot 
	   \frac{W_4}{W_{3}}\cdot 
	   \frac{W_5}{W_{4}}\cdot 
	   \frac{W_6}{W_{5}}\cdot 
	   \frac{W_7}{W_{6}}\cdot 
	   \frac{W_8}{W_{7}}\cdot 
	   \frac{W_9}{W_{8}}\cdot 
	   \frac{W_{10}}{W_{9}}\cdot 
	   \frac{W_{fianl}}{W_{10}} 
	   \label{eqn:Equation1.5} 
\end{equation}

\bigskip
From this point on, it’s necessary to have a table, like the one of table~\ref{table:Table1}, in which all weight ratios can be found statistically except for cruise, alternate cruise and loiter phases; these three unknown ratios are those that build up the Breguet weight ratio required by the range formula.   

\begin{table}[!ht]
\centering
\makebox[\linewidth]{
\begin{tabular}{p{2.7cm}p{2cm}p{1.1cm}p{1.7cm}p{2.7cm}p{1.2cm}p{2.1cm}}
\toprule
\textbf{Airplane type} & \textbf{Start}, \textbf{Warm-up} & \textbf{Taxi} & \textbf{Take-off} & \textbf{Brief descent} & \textbf{Brief climb} & \textbf{Landing, Taxi and Shutdown} \\
\midrule
Homebuilts                     & 0,998 & 0,998 & 0,998 & 0,995 & 0,995 & 0,995       \\[0.7cm]
Single Engine                  & 0,995 & 0,997 & 0,998 & 0,992 & 0,993 & 0,993       \\[0.7cm]
Twin Engine                    & 0,992 & 0,996 & 0,996 & 0,990 & 0,992 & 0,992       \\[0.7cm]
Agricultural                   & 0,996 & 0,995 & 0,996 & 0,998 & 0,999 & 0,998       \\[0.7cm]
Business Jets                  & 0,990 & 0,995 & 0,995 & 0,980 & 0,990 & 0,992       \\[0.7cm]
Regional TBP's                 & 0,990 & 0,995 & 0,995 & 0,985 & 0,985 & 0,995       \\[0.7cm]
Transport Jets                 & 0,990 & 0,990 & 0,995 & 0,980 & 0,990 & 0,992       \\[0.7cm]
Mil. Trainers                  & 0,990 & 0,990 & 0,990 & 0,980 & 0,990 & 0,995       \\[0.7cm]
Fighters                       & 0,990 & 0,990 & 0,990 & 0,960-0,900 & 0,990 & 0,995     \\[0.7cm]
Mil.Patrol,Bmb and Trspt       & 0,990 & 0,990 & 0,995 & 0,980 & 0,990 & 0,992       \\[0.7cm]
Flying boats,Amph. and Floats  & 0,992 & 0,990 & 0,996 & 0,985 & 0,990 & 0,990       \\[1.2	cm]
Supersonic Cruise              & 0,990 & 0,995 & 0,995 & 0,920-0,870 & 0,985 & 0,992     \\[0.1	cm]
\bottomrule
\end{tabular}
}
\caption{Suggested fuel fractions}
\label{table:Table1}
\end{table}


For the evaluation of point C, similar steps have to be followed with the difference that, in this case, the fuel is the maximum that the wing tanks can store and the payload is the unknown of the (\ref{eqn:Equation1.6}).

\begin{equation}
W_{TO,max}=W_{OE}+W_{payload}+W_{fuel,max}
\label{eqn:Equation1.6}
\end{equation}

\bigskip
From this equation it’s possible to calculate point C ordinate, while for the range abscissa the same procedure used for point B range has to be followed with the difference that now it’s necessary to use the fuel weight relative to the maximum fuel tank capacity which is known from the wing fuel tank design.
  
Finally for point D, the payload is set at zero so that is possible to evaluate the WTO, relative to maximum fuel weight with no passengers, from the following equation.

\begin{equation}
W_{TO}=W_{OE}+W_{fuel,max}
\label{eqn:Equation1.7}
\end{equation}

\bigskip
This new WTO generates a lower $M_{ff}$, from fuel fraction equation, which leads to a bigger weight ratio to be used in Breguet formulas with the result of a bigger range.

\section{Java class architecture}
In this paragraph the implementation in the JPAD software of the Payload-Range diagram, through the \lstinline[language=Java]!calcPayloadRange! Java class, is presented. The idea has been to create a dedicated Java class which is demanded of the calculation of the four couple of range and payload values presented before; moreover it has to confront the user chosen Mach number condition with the best range one and to parametrize the analysis at different maximum take-off weight in order to help users making design decisions about different version of the same aircraft.

The class core consists of the following three principal methods which have to evaluate points B, C and D coordinates using the procedure explained before.

\begin{itemize}
\item\lstinline[language=Java]!calcRangeAtMaxPayload!
\item\lstinline[language=Java]!calcRangeAtMaxFuel!
\item\lstinline[language=Java]!calcRangeAtZeroPayload!
\end{itemize}

\bigskip
Each of these requires in input the table~\ref{table:Table2} data, a database which collects all data from table~\ref{table:Table1},  named \emph{FuelFractions.h5}, and an engine database, for turboprop or turbofan, in which all specific fuel consumption values, at given Mach number and altitude, are stored.

\begin{table}[!ht]
\begin{tabular}{p{5cm}p{8cm}}
\toprule
\lstinline[language=Java]!maxTakeOffMass! & Maximum take-off mass \\[0.1	cm]
\lstinline[language=Java]!sweepHalfChordEquivalent! & Equivalent wing sweep angle at half chord \\[0.1cm]
\lstinline[language=Java]!surface! & Wing surface \\[0.1cm]
\lstinline[language=Java]!cd0!	& Wing cD0 \\[0.1cm]
\lstinline[language=Java]!oswald!	& Wing oswald factor \\[0.1cm]
\lstinline[language=Java]!cl!	& The current lift coefficient in cruise configuration \\[0.1cm]
\lstinline[language=Java]!ar!	& Wing aspect ratio \\[0.1cm]
\lstinline[language=Java]!tcMax! & Mean maximum thickness of the wing \\[0.1cm]
\lstinline[language=Java]!byPassRatio! & Engine by-pass ratio (when needed) \\[0.1cm]
\lstinline[language=Java]!eta! & Propeller efficiency (when needed) \\[0.1cm]
\lstinline[language=Java]!altitude! & Cruise altitude \\[0.1cm]
\lstinline[language=Java]!currentMach! & The actual Mach number during cruise \\[0.1cm]
\lstinline[language=Java]!isBestRange! & A boolean variable that is true if the evaluation of the best range condition is performed, otherwise it's false         \\
\bottomrule
\end{tabular}
\caption{Input data}
\label{table:Table2}
\end{table}

From this point on, the evaluation of (\ref{eqn:Equation1.3}), (\ref{eqn:Equation1.6}) and (\ref{eqn:Equation1.7}), for each method, and of (\ref{eqn:Equation1.4}), for all of them, is performed in order to set up the following calculations and to obtain the unknown value of the payload in point~C case; after that the fuel fractions database is read in order to obtain all known data necessary to calculate the required weight ratio to be used in (\ref{eqn:Equation1.1}) or (\ref{eqn:Equation1.2}) depending on the aircraft engine type. It’s important to highlight that the database reading process is made up so that it can recognize all different kind of aircraft from table~\ref{table:Table1} and read the related line values.

\bigskip
Since each method has to compare the chosen Mach number condition with the best range one, the boolean variable presented before is used in order to distinguish between different application cases; in this way, when the user specify the aircraft engine type and the boolean variable, the method reads the specific fuel consumption value, at given Mach number and altitude, from the related engine database and performs the calculation of the lift and drag wing coefficients which are then used to obtain the aerodynamic efficiency value. At this point is possible to calculate the range that represents point B, C or D abscissa. 

It should be noted that, in case of a true value of the boolean variable, the evaluation of the lift and drag wing coefficients, as well as the best range Mach number that replaces the user chosen one, is performed by calculating the parabolic drag polar characteristic points and choosing those that maximizes the range (point E for the turboprop and point A for the turbofan); whereas for a false value of the boolean variable, the lift coefficient is calculated by using the current flight condition angle of attack into the linear part of the wing lift curve, while the drag coefficient is evaluated from the aircraft total drag polar taking also into account potential wave drag sources.   

\bigskip
Now that all points coordinates are known, the two following methods are demanded to build up abscissas and ordinates values arrays to be used in plotting the diagram.

\begin{itemize}
\item\lstinline[language=Java]!createRangeArray!
\item\lstinline[language=Java]!createPayloadArray!
\end{itemize}

\bigskip
They use the Java \lstinline[language=Java]!List! interface to generate an ordered collection of values, also known as \emph{sequence}, which are then populated with the following values.

\begin{table}[!hb]
\begin{tabular}{cc}
\toprule
\textbf{Range Array} & \textbf{Payload Array} \\ 
\midrule
0,0	& Maximum payload, in number of passengers \\ [0.2cm]
\lstinline[language=Java]!calcRangeAtMaxPayload! output &	Maximum payload, in number of passengers \\ [0.2cm]
\lstinline[language=Java]!calcRangeAtMaxFuel! output &	Payload calculated in \lstinline[language=Java]!calcRangeAtMaxFuel! \\ [0.2cm]
\lstinline[language=Java]!calcRangeAtZeroPayload! output &	0,0 \\ [0.2cm]
\bottomrule
\end{tabular}
\caption{Payload and range array components}
\label{table:Table3}
\end{table}

\bigskip
Finally the two arrays are used as input, together with another one from the best range condition analysis, for the last method that has to plot the diagram; this is called \lstinline[language=Java]!createPayloadRangeCharts_Mach! and uses the \lstinline[language=Java]!JFreeChart! Java library to generate a .png output image into the output folder of the software and is also able to create a .tikz version of the diagram to be used in \LaTeX.

As said before, this class finds another purpose in parameterizing the diagram in different maximum take-off weight conditions.

To do that, the three core methods are used again inside a new one, named \lstinline[language=Java]!createPayloadRangeMatrices!, which implements a recursive calculation of the three points coordinates at different maximum take-off mass conditions generated by decreasing the original mass by 5$\%$ until it reaches a total decrease of 20$\%$. This new method requires the same input data reported in table~\ref{table:Table2} except for the maximum take-off mass which is generated inside the method itself.

The output matrices are then used in a plotting method, equivalent to previous one but named \lstinline[language=Java]!createPayloadRangeCharts_MaxTakeOffMass!, which generates the .png and .tikz output images by receiving as input two matrices and not two arrays as before.

\begin{figure}[!ht]
\centering
\includegraphics[keepaspectratio, width=0.6\textwidth]{PayloadRange_BestRange_Flowchart}
\caption{Payload-Range java class flowchart for best range and chosen Mach conditions comparison}
\label{fig:Figure3}
\end{figure}

\bigskip
\begin{figure}[!hb]
\centering
\includegraphics[keepaspectratio, width=0.6\textwidth]{PayloadRange_MTOM_Flowchart}
\caption{Payload-Range java class flowchart for maximum take-off mass parameterization}
\label{fig:Figure4}
\end{figure}

\section{Case study: ATR-72 and B747-100B}
With the purpose of validating calculations presented before, two case studies have been taken into account; the first one is on the ATR-72 and the second one on the B747-100B.

\bigskip
The ATR-72, made by the French-Italian aircraft manufacturer ATR, is a stretched variant of the original ATR-42 that entered in service in 1989; it’s powered by two turboprop engine and it’s used typically as a regional airliner. The main purpose of its design was to increase the seating capacity throughout the stretching of the fuselage by 4.5m, an increased wingspan, more powerful engines and an increased fuel capacity by approximately 10$\%$.

\bigskip
The Boeing 747 is a wide-body commercial jet airliner and cargo aircraft, often referred to by its original nickname, Jumbo Jet, or Queen of the Skies. Its distinctive hump upper deck along the forward part of the aircraft makes it among the world's most recognizable aircraft, and it was the first wide-body produced. Manufactured by Boeing's Commercial Airplane unit in the United States, the original version of the 747 had two and a half times greater capacity than the Boeing 707, one of the common large commercial aircraft of the 1960s. The four-engine 747 uses a double deck configuration for part of its length and it’s available in passenger and other versions. Boeing designed the 747's hump-like upper deck to serve as a first class lounge or extra seating, and to allow the aircraft to be easily converted to a cargo carrier by removing seats and installing a front cargo door. The 747-100B model was developed from the 747-100SR, using its stronger airframe and landing gear design; the type had an increased fuel capacity of $\SI{182000}{\liter}$, allowing for a 5000 nautical mile range with a typical 452 passengers payload, and an increased maximum take-off weight of $\SI{340000}{\kilogram}$ was offered; unlike the original 747-100, the 747-100B was offered with Pratt \& Whitney JT9D-7A, General Electric CF6-50, or Rolls-Royce RB211-524 turbofan engines.

\bigskip
The two presented tests share the same architecture so that a general guideline can be followed; this strategy has been designed with the purpose of making a general procedure in order to simplify user’s work.

For more clarity, listings of the two test types are reported, as an exemplificative practice, only in the case of the ATR-72 turboprop aircraft. 

\bigskip
First of all it’s necessary to set up all default folders in order to make them achievable from any location inside the software; this is done with the static method \lstinline[language=Java]!initWorkingDirectoryTree! of the \lstinline[language=Java]!MyConfiguration! class located in \lstinline[language=Java]!JPADConfigs! package. In this way, every time the user wants to point at a specific folder, like the input or output directory, it’s necessary only to call the static method \lstinline[language=Java]!getDir!, also from \lstinline[language=Java]!MyConfiguration!  class, which reads the~folder name, from a dedicated enumeration, and associate it with the related directory from a \lstinline[language=Java]!HashMap! named \lstinline[language=Java]!mapPaths!.

The second step is to read the engine and the fuel fractions databases with the related java class reader; this particular class type uses the super class \lstinline[language=Java]!DatabaseReader!, which implements the use of \lstinline[language=Java]!MyHDFReader! class, to access an~.h5 dataset values. For more information about how to build and use an HDF database, see Appendix ??.

\bigskip
\begin{lstlisting}[caption={Excerpt of the ATR-72 Payload-Range test - preliminary steps}, captionpos=b, tabsize=2]
public class PayloadRange_Test_TP{
	//-----------------------------------------------------
	// MAIN:
	public static void main(String[] args) throws
			HDF5LibraryException, NullPointerException{

		System.out.println("-----------------------------");
		System.out.println("PayloadRangeCalc_Test :: main");
		System.out.println("-----------------------------");
		System.out.println(" ");
		//--------------------------------------------------
		// Assign all default folders
		MyConfiguration.initWorkingDirectoryTree();
		//--------------------------------------------------
		// Setup database(s)	
		String databaseFolderPath = MyConfiguration
						.getDir(FoldersEnum.DATABASE_DIR);
		String aerodynamicDatabaseFileName = 
						"Aerodynamic_Database_Ultimate.h5";
		String fuelFractionDatabaseFileName = 
						"FuelFractions.h5";
		AerodynamicDatabaseReader aeroDatabaseReader = 
						new AerodynamicDatabaseReader(
										databaseFolderPath,
										aerodynamicDatabaseFileName
										);
		FuelFractionDatabaseReader fuelFractionReader =
						new FuelFractionDatabaseReader(
										databaseFolderPath,
										fuelFractionDatabaseFileName
										);
\end{lstlisting}

Now that all required resources are set up, following steps are to create the aircraft model, assign its operating conditions and perform all necessary analysis like the aerodynamic one. This is done throughout the use of the following three fundamental classes, which modes of operation are explained in the flowchart of figure~\ref{fig:Figure5}.

\begin{itemize}
\item\lstinline[language=Java]!Aircraft! class
\item\lstinline[language=Java]!OperatingConditions! class
\item\lstinline[language=Java]!ACAnalysisManager! class
\end{itemize}

\begin{lstlisting}[caption={Excerpt of the ATR-72 Payload-Range test - Aircraft, OperatingConditions and ACAnalysisManager setup}, captionpos=b, tabsize=2]
		//--------------------------------------------------
		// Operating Condition / Aircraft / AnalysisManager 
		// (geometry calculations)
		OperatingConditions theCondition = new OperatingConditions();
		
		Aircraft aircraft = Aircraft
						.createDefaultAircraft("ATR-72");
		aircraft.get_theAerodynamics()
						.set_aerodynamicDatabaseReader(
										aeroDatabaseReader
										);
		aircraft.get_theFuelTank()
						.setFuelFractionDatabase(
										fuelFractionReader
										);
		aircraft.set_name("ATR-72");
		aircraft.get_wing()
						.set_theCurrentAirfoil(
								new MyAirfoil(
										aircraft.get_wing(), 
										0.5
										)
								);		
		
		ACAnalysisManager theAnalysis = 
						new ACAnalysisManager(
										theCondition
										);
		theAnalysis.updateGeometry(aircraft);
		
		
		//--------------------------------------------------
		// Set the CoG(Bypass the Balance analysis allowing
		// to perform Aerodynamic analysis only)
		CenterOfGravity cgMTOM = new CenterOfGravity();

		// x_cg in body-ref.-frame
		cgMTOM.set_xBRF(Amount.valueOf(12.0, SI.METER)); 
		cgMTOM.set_yBRF(Amount.valueOf(0.0, SI.METER));
		cgMTOM.set_zBRF(Amount.valueOf(2.3, SI.METER));

		aircraft.get_theBalance().set_cgMTOM(cgMTOM);
		aircraft.get_HTail().calculateArms(aircraft);
		aircraft.get_VTail().calculateArms(aircraft);
		
		theAnalysis.doAnalysis(
						aircraft, 
						AnalysisTypeEnum.AERODYNAMIC);
\end{lstlisting}

\bigskip
All data required by the \lstinline[language=Java]!calcPayloadRange! class are now ready to be used and it’s possible to create an istance of this class accessing, in this way, to all its methods, which have been explained in the previous paragraph. 

\bigskip
\begin{lstlisting}[caption={Excerpt of the ATR-72 Payload-Range test - Payload-Range class istance creation}, captionpos=b, tabsize=2]
		//--------------------------------------------------
		// Creating the Calculator Object
		PayloadRangeCalc test = new PayloadRangeCalc(
								theCondition, 
								aircraft,
								AirplaneType.TURBOPROP_REGIONAL
								);
\end{lstlisting}

\bigskip
The first check that is implemented has the purpose of inspect wether or not the chosen cruise Mach number is bigger than the crest critical one; in this case a warning message is launched in order to inform the user of the situation. The method demanded of this is \lstinline[language=Java]!checkCriticalMach! which accepts in input a given Mach number, compares it with the one calculated with Kroo method, starting from the cruise lift coefficient, and return a boolean variable that is true only if the chosen Mach number is bigger than the crest critical one indeed. 

\bigskip
\begin{lstlisting}[caption={Excerpt of the ATR-72 Payload-Range test - critical Mach number check}, captionpos=b, tabsize=2]
		// ----------CRITICAL MACH NUMBER CHECK--------------
		boolean check = test
						.checkCriticalMach(
										theCondition.get_machCurrent()
										);
		
		if (check)
			System.out.println("\n\n-------------------------"
			+"\nCurrent Mach lower then critical Mach number"
			+"\nCurrent Mach = "+theCondition.get_machCurrent() 
			+"\nCritical Mach = "+test.getCriticalMach() 
			+"\n\n\t CHECK PASSED -> PROCEDING TO CALCULATION "
			+"\n\n"
			+"-----------------------------------------------");
		else{
			System.err.println("\n\n-------------------------"
			+"\nCurrent Mach bigger then critical Mach number"
			+"\nCurrent Mach = "+theCondition.get_machCurrent() 
			+"\nCritical Mach = "+test.getCriticalMach() 
			+"\n\n\t CHECK NOT PASSED -> WARNING!!! "
			+"\n\n"
			+"--------------------------------------");
		}
\end{lstlisting}

\bigskip
\begin{lstlisting}[caption={Excerpt of the ATR-72 Payload-Range test results - critical Mach number check}, captionpos=b, tabsize=2]
---------------------------------------------------------
Current Mach is lower then critical Mach number.
Current Mach = 0.43
Critical Mach = 0.6659791543529567

	 CHECK PASSED --> PROCEDING TO CALCULATION 
---------------------------------------------------------
\end{lstlisting}

\bigskip
\begin{lstlisting}[caption={Excerpt of the B747-100B Payload-Range test results - critical Mach number check}, captionpos=b, tabsize=2]
---------------------------------------------------------
Current Mach is lower then critical Mach number.
Current Mach = 0.84
Critical Mach = 0.8490814607347361

	 CHECK PASSED --> PROCEDING TO CALCULATION 
---------------------------------------------------------
\end{lstlisting}

\bigskip
At this point, if the user wants to compare the chosen Mach number condition with the best range one, all that it’s needed is to invoke the \lstinline[language=Java]!createRangeArray! method, for both the operative conditions, and the \lstinline[language=Java]!createPayloadArray! one; after that the diagram is generated from the method \lstinline[language=Java]!createPayloadRangeCharts_Mach!.

\bigskip
\begin{lstlisting}[caption={Excerpt of the ATR-72 Payload-Range test - Payload-Range diagram evaluation and plot}, captionpos=b, tabsize=2]
		// ----------------BEST RANGE CASE-------------------
		System.out.println();
		System.out.println("-------BEST RANGE CASE--------");
		
		List<Amount<Length>> vRange_BR = test
						.createRangeArray(
										test.getMaxTakeOffMass(),
										test.getSweepHalfChordEquivalent(),
										test.getSurface(),
										test.getCd0(),
										test.getOswald(),
										aircraft.get_theAerodynamics().getcLE(),
										test.getAr(),
										test.getTcMax(),
										test.setByPassRatio(0.0),
										test.getEta(),
										test.getAltitude(),
										test.calculateBestRangeMach(
														EngineTypeEnum.TURBOPROP,
														test.getSurface(),
														test.getAr(),
														test.getOswald(),
														test.getCd0(),
														test.getAltitude()),
										true);
				
		// -------------USER CURRENT MACH-------------------
		System.out.println();
		System.out.println("------CURRENT MACH CASE-------");
		
		List<Amount<Length>> vRange_CM = test
						.createRangeArray(
										test.getMaxTakeOffMass(),
										test.getSweepHalfChordEquivalent(),
										test.getSurface(),
										test.getCd0(),
										test.getOswald(),
										test.getCl(),
										test.getAr(),
										test.getTcMax(),
										test.setByPassRatio(0.0),
										test.getEta(),
										test.getAltitude(),
										test.getCurrentMach(),
										false);
		
		// -------------------PLOTTING----------------------	
		// Mach parameterization:
		List<Double> vPayload = test.createPayloadArray();
		test.createPayloadRangeCharts_Mach(
						vRange_BR,
						vRange_CM,
						vPayload,
						test.calculateBestRangeMach(
										EngineTypeEnum.TURBOPROP,
										test.getSurface(),
										test.getAr(),
										test.getOswald(),
										test.getCd0(),
										test.getAltitude()),
						test.getCurrentMach());
	}
	//--------------------------------------------------
	// END OF THE TEST
} 
\end{lstlisting}

Otherwise, if it’s the maximum take-off mass parameterization the wanted analysis, the required call is for \lstinline[language=Java]!createPayloadRangeMatrices! method followed by the plotting method \lstinline[language=Java]!createPayloadRangeCharts_MaxTakeOffMass!. 

\bigskip
\begin{lstlisting}[caption={Excerpt of the ATR-72 Payload-Range test - maximum take-off mass parameterization}, captionpos=b, tabsize=2]
		// -----------------MTOM PARAMETERIZATION----------
		test.createPayloadRangeMatrices(
						test.getSweepHalfChordEquivalent(),
						test.getSurface(),
						test.getCd0(),
						test.getOswald(),
						test.getCl(),
						test.getAr(),
						test.getTcMax(),
						test.setByPassRatio(0.0),
						test.getEta(),
						test.getAltitude(),
						test.getCurrentMach(),
						false
						);
		// ------------------PLOTTING----------------------		
		// MTOM parameterization:
		test.createPayloadRangeCharts_MaxTakeOffMass(
						test.getRangeMatrix(),
						test.getPayloadMatrix()
						);
	}
} 
\end{lstlisting}

\bigskip
In conclusion, following pages shows a summary of input data used for each analyzed aircraft, together with their related results in terms of Payload-Range diagrams for both the chosen Mach number and best range comparison and max take-off mass parameterization. 

\begin{figure}
  \begin{adjustbox}{addcode={\begin{minipage}{\width}}{\caption{%
     \lstinline[language=Java]!Aircraft!, \lstinline[language=Java]!OperatingConditions! and \lstinline[language=Java]!ACAnalysisManager! flowchart}
     \label{fig:Figure5}
     \end{minipage}},rotate=90,center}
      \includegraphics[scale=.22]{HowToCreateADefaultAircraftInJPAD}
  \end{adjustbox}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[keepaspectratio, width=0.62\textwidth]{ATR-72}
\caption{ATR-72 views – Jane's All the World's Aircraft 2004-2005}
\label{fig:Figure6}
\end{figure}

\begin{table}[!hb]
\makebox[\linewidth]{
\begin{tabular}{p{4cm}p{7.5cm}p{2cm}}
\toprule
\textbf{Variables} & \textbf{Description} & \textbf{Values} \\
\midrule
\lstinline[language=Java]!altitude! & Cruise altitude & $\SI{6000}{\meter}$ \\[0.2cm]
\lstinline[language=Java]!machCurrent! & The user chosen Mach number & 0,43 \\[0.2cm]
\lstinline[language=Java]!surface! & Wing surface & $\SI{61}{\meter\squared}$ \\[0.2cm]
\lstinline[language=Java]!taperRatioEquivalent! & Taper ratio of the equivalent wing & 0,545   \\[0.2cm]
\lstinline[language=Java]!maxTakeOffMass! & maximum take-off mass & $\SI{23063.5789}{\kilogram}$         \\[0.2cm]
\lstinline[language=Java]!operatingEmptyMass!	& operating empty mass & $\SI{12935.5789}{\kilogram}$ \\[0.2cm]
\lstinline[language=Java]!maxFuelMass! & maximum fuel mass & $\SI{5000.0}{\kilogram}$ \\[0.2cm]
\lstinline[language=Java]!nPassMax! & maximum number of passengers & 72 \\[0.2cm]
\lstinline[language=Java]!sweepLEEquivalent! & Equivalent wing leading edge sweep angle & $\SI{3.1415}{\degree}$ \\[0.2cm]
\lstinline[language=Java]!oswald! & Wing oswald factor & 0,7585 \\[0.2cm]
\lstinline[language=Java]!byPassRatio! & Engine bypass ratio & 0,0 \\[0.2cm]
\lstinline[language=Java]!eta! & propeller efficiency & 0,85 \\[0.2cm]
\lstinline[language=Java]!tcMax! & Mean maximum thickness of the wing & 0,1675 \\[0.2cm]
\lstinline[language=Java]!cl! & The cruise lift coefficient & 0,45 \\[0.2cm]
\lstinline[language=Java]!ar! & Wing aspect ratio & 12 \\[0.2cm]
\lstinline[language=Java]!cd0! & Wing parasite drag coefficient & 0,0317 \\[0.2cm]
\bottomrule
\end{tabular}
}
\caption{ATR-72 input data}
\label{table:Table4}
\end{table}

\begin{figure}[!h]
\centering
\input{../../images/PayloadRange_Mach_TP.tikz}
\caption{ATR-72 Payload-Range - Chosen Mach number and best range comparison}
\end{figure}

\begin{figure}[!h]
\centering
\input{../../images/PayloadRange_MaxTakeOffMass_TP.tikz}
\caption{ATR-72 Payload-Range - Maximum take-off mass parameterization}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[keepaspectratio, width=0.62\textwidth]{B747-100B}
\caption{B747-100B views – Jane's All the World's Aircraft 2004-2005}
\label{fig:Figure7}
\end{figure}

\begin{table}[!hb]
\makebox[\linewidth]{
\begin{tabular}{p{4cm}p{7.5cm}p{2cm}}
\toprule
\textbf{Variables} & \textbf{Description} & \textbf{Values} \\
\midrule
\lstinline[language=Java]!altitude! & Cruise altitude & $\SI{11000}{\meter}$ \\[0.2cm]
\lstinline[language=Java]!machCurrent! & The user chosen Mach number & 0,83 \\[0.2cm]
\lstinline[language=Java]!surface! & Wing surface & $\SI{511}{\meter\squared}$ \\[0.2cm]
\lstinline[language=Java]!taperRatioEquivalent! & Taper ratio of the equivalent wing & 0,284   \\[0.2cm]
\lstinline[language=Java]!maxTakeOffMass! & maximum take-off mass & $\SI{354991.5060}{\kilogram}$         \\[0.2cm]
\lstinline[language=Java]!operatingEmptyMass!	& operating empty mass & $\SI{153131.9860}{\kilogram}$ \\[0.2cm]
\lstinline[language=Java]!maxFuelMass! & maximum fuel mass & $\SI{147409.52}{\kilogram}$ \\[0.2cm]
\lstinline[language=Java]!nPassMax! & maximum number of passengers & 550 \\[0.2cm]
\lstinline[language=Java]!sweepLEEquivalent! & Equivalent wing leading edge sweep angle & $\SI{38.4288}{\degree}$ \\[0.2cm]
\lstinline[language=Java]!oswald! & Wing oswald factor & 0,6277 \\[0.2cm]
\lstinline[language=Java]!byPassRatio! & Engine bypass ratio & 5,0 \\[0.2cm]
\lstinline[language=Java]!eta! & propeller efficiency & 0,0 \\[0.2cm]
\lstinline[language=Java]!tcMax! & Mean maximum thickness of the wing & 0,1292 \\[0.2cm]
\lstinline[language=Java]!cl! & The cruise lift coefficient & 0,45 \\[0.2cm]
\lstinline[language=Java]!ar! & Wing aspect ratio & 6,9 \\[0.2cm]
\lstinline[language=Java]!cd0! & Wing parasite drag coefficient & 0,0182 \\[0.2cm]
\bottomrule
\end{tabular}
}
\caption{B747-100B input data}
\label{table:Table5}
\end{table}

\begin{figure}[!h]
\centering
\input{../../images/PayloadRange_Mach_TF.tikz}
\caption{ATR-72 Payload-Range - Chosen Mach number and best range comparison}
\end{figure}

\begin{figure}[!h]
\centering
\input{../../images/PayloadRange_MaxTakeOffMass_TF.tikz}
\caption{ATR-72 Payload-Range - Maximum take-off mass parameterization}
\end{figure}

\end{document}