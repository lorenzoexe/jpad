clc; close all; clear all;

%% Import data
EtaDeltaSingleSlotted = importdata('EtaDeltaSimpleHinged.mat');
EtaDeltaDoubleSlotted = importdata('EtaDeltaDoubleSlotted.mat');
EtaDeltaSplit = importdata('EtaDeltaSimpleHinged.mat');
EtaDeltaPlain = importdata('EtaDeltaSimpleHinged.mat');
EtaDeltaFowler = importdata('EtaDeltaFowler.mat');
EtaDeltaTripleSlotted = importdata('EtaDeltaTripleSlotted.mat');

nPoints = 50;
deltafVector = transpose(linspace(0, 80, nPoints));

%% Single Slotted (1)
smoothingParameter = 0.999999;
EtaDeltaSingleSlotted_SplineStatic = csaps( ...
    EtaDeltaSingleSlotted(:,1), ...
    EtaDeltaSingleSlotted(:,2), ...
    smoothingParameter ...
    );

EtaDeltaSingleSlotted_Static = ppval( ...
    EtaDeltaSingleSlotted_SplineStatic, ...
    deltafVector ...
    );

%% Double Slotted  (2)
smoothingParameter = 0.999999;
EtaDeltaDoubleSlotted_SplineStatic = csaps( ...
    EtaDeltaDoubleSlotted(:,1), ...
    EtaDeltaDoubleSlotted(:,2), ...
    smoothingParameter ...
    );

EtaDeltaDoubleSlotted_Static = ppval( ...
    EtaDeltaDoubleSlotted_SplineStatic, ...
    deltafVector ...
    );

%% Split Flap (3)
smoothingParameter = 0.999999;
EtaDeltaSplit_SplineStatic = csaps( ...
    EtaDeltaSplit(:,1), ...
    EtaDeltaSplit(:,2), ...
    smoothingParameter ...
    );

EtaDeltaSplit_Static = ppval( ...
    EtaDeltaSplit_SplineStatic, ...
    deltafVector ...
    );

%% Plain Flap (4)
smoothingParameter = 0.999999;
EtaDeltaPlain_SplineStatic = csaps( ...
    EtaDeltaPlain(:,1), ...
    EtaDeltaPlain(:,2), ...
    smoothingParameter ...
    );

EtaDeltaPlain_Static = ppval( ...
    EtaDeltaPlain_SplineStatic, ...
    deltafVector ...
    );

%% Fowler  (5)
smoothingParameter = 0.999999;
EtaDeltaFowler_SplineStatic = csaps( ...
    EtaDeltaFowler(:,1), ...
    EtaDeltaFowler(:,2), ...
    smoothingParameter ...
    );

EtaDeltaFowler_Static = ppval( ...
    EtaDeltaFowler_SplineStatic, ...
    deltafVector ...
    );

%% Triple Soltted  (6)
smoothingParameter = 0.999999;
EtaDeltaTripleSlotted_SplineStatic = csaps( ...
    EtaDeltaTripleSlotted(:,1), ...
    EtaDeltaTripleSlotted(:,2), ...
    smoothingParameter ...
    );

EtaDeltaTripleSlotted_Static = ppval( ...
    EtaDeltaTripleSlotted_SplineStatic, ...
    deltafVector ...
    );

%% Plots
figure(1)

plot ( ...
    deltafVector(1:33), EtaDeltaSingleSlotted_Static(1:33), 'b-*' ... , ...
 );

hold on;

plot ( ...
    deltafVector(1:41), EtaDeltaDoubleSlotted_Static(1:41), 'b-+' ... , ...
 );

hold on;

plot ( ...
    deltafVector(1:33), EtaDeltaSplit_Static(1:33), 'b-o' ... , ...
 );

hold on;

plot ( ...
    deltafVector(1:33), EtaDeltaPlain_Static(1:33), 'b-.' ... , ...
 );

hold on;

plot ( ...
    deltafVector(1:29), EtaDeltaFowler_Static(1:29), 'b-^' ... , ...
 );

hold on;

plot ( ...
    deltafVector(1:45), EtaDeltaTripleSlotted_Static(1:45), 'b' ... , ...
 );

xlabel('\delta_f'); ylabel('\eta_\delta');
 title('\eta_\delta variation with \delta_f');
 legend('1-Slot', '2-Slot','Split Flap', 'Plain Flap', 'Fowler', '3-Slot');
 
 axis([0 80 0.3 1]);
 grid on;
 %% preparing output to HDF
 
% FlapType (see number for each section)
flapTypeVector = [ ...
    1;2;3;4;5;6 ...
    ]; 
 
%columns --> curves
myData = [ ...
    EtaDeltaSingleSlotted_Static, ...
    EtaDeltaDoubleSlotted_Static, ...
    EtaDeltaSplit_Static, ...
    EtaDeltaPlain_Static, ...
    EtaDeltaFowler_Static, ...
    EtaDeltaTripleSlotted_Static
    ];    
hdfFileName = 'EtaDelta_vs_DeltaFlap.h5';

if ( exist(hdfFileName, 'file') )
    fprintf('file %s exists, deleting and creating a new one\n', hdfFileName);
    delete(hdfFileName)
else
    fprintf('Creating new file %s\n', hdfFileName);
end

% Dataset: data
h5create(hdfFileName, '/EtaDelta_vs_DeltaFlap/data', size(myData'));
h5write(hdfFileName, '/EtaDelta_vs_DeltaFlap/data', myData');

% Dataset: var_0
h5create(hdfFileName, '/EtaDelta_vs_DeltaFlap/var_0', size(flapTypeVector'));
h5write(hdfFileName, '/EtaDelta_vs_DeltaFlap/var_0', flapTypeVector');

% Dataset: var_1
h5create(hdfFileName, '/EtaDelta_vs_DeltaFlap/var_1', size(deltafVector'));
h5write(hdfFileName, '/EtaDelta_vs_DeltaFlap/var_1', deltafVector');