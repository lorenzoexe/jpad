package configuration.enumerations;

/**
 * Aircraft types as recognized by the application 
 * 
 * @author LA
 *
 */
public enum AircraftTypeEnum {

	JET,
	FIGHTER,
	BUSINESS_JET,
	TURBOPROP, 
	GENERAL_AVIATION;

}