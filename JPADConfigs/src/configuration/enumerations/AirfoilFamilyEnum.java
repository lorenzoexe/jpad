package configuration.enumerations;

/**
 * Airfoil types as recognized by the application
 * 
 * @author LA
 *
 */
public enum AirfoilFamilyEnum{
	NACA63_209,
	NACA64_208,
	NACA65_209,
	NACA66_209,
	NACA63_412,
	NACA64_412,
	NACA65_410;
}