package configuration.enumerations;

/**
 * Airfoil type relative to the behaviour at high subsonic Mach number
 *  
 * @author LA
 *
 */
public enum AirfoilTypeEnum {
	CONVENTIONAL,
	PEAKY,
	SUPERCRITICAL,
	MODERN_SUPERCRITICAL;
}