package configuration.enumerations;

/**
 * Engine operating conditions supported by the application
 * 
 * @author LA
 *
 */
public enum EngineOperatingConditionEnum {
	TAKE_OFF,
	CONTINUOUS,
	CLIMB,
	CRUISE,
	DESCENT;
}
