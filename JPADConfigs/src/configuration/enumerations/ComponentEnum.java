package configuration.enumerations;

/**
 * The aircraft components that can be created by the application
 * to make up an aircraft
 *  
 * @author LA
 *
 */
public enum ComponentEnum{
	ALL,
	UNDEFINED,
	WING,
	HORIZONTAL_TAIL, 
	VERTICAL_TAIL,
	CANARD,
	FUSELAGE,
	NACELLE,
	LANDING_GEAR,
	POWER_PLANT,
	FUEL_TANK,
	SYSTEMS, 
	ENGINE;
}