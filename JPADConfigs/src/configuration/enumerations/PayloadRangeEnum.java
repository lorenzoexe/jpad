package configuration.enumerations;

public enum PayloadRangeEnum {
	
	// input
	AirplaneType,
	EngineType,
	AirfoilType,
	Altitude,
	Mach_number,
	Planform_surface,
	TaperRatio,
	Maximum_take_off_mass,
	Operating_empty_mass,
	Maximum_fuel_mass,
	Maximum_number_of_passengers,
	SweepLE,
	OswaldFactor,
	ByPassRatio,
	Propeller_efficiency,
	Mean_maximum_thickness,
	Current_lift_coefficient, 
	AspectRatio,
	CD0,
	// output
	Design_Range,
	Max_Fuel_Range,
	Max_Range,
	Design_Payload,
	SFC,
	Efficiency

}
