package configuration.enumerations;

public enum WindshieldType {
	FLAT_PROTRUDING,
	FLAT_FLUSH,
	SINGLE_ROUND, 
	SINGLE_SHARP,
	DOUBLE;
}
