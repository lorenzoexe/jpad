package configuration.enumerations;

/**
 * Engine positions as recognized by the application
 * 
 * @author LA
 *
 */
public enum EngineMountingPositionEnum {
	BURIED,
	WING,
	AFT_FUSELAGE,
	REAR_FUSELAGE;
}