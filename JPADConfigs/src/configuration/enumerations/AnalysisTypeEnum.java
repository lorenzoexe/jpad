package configuration.enumerations;

/** 
 * The kind of analyses which are (or will be) supported by the application
 * 
 * @author LA
 * 
 */
public enum AnalysisTypeEnum {
	AERODYNAMIC,
	BALANCE,
	DYNAMICS,
	GEOMETRY,
	PERFORMANCES,
	PROPULSION,
	STABILITY,
	STRUCTURES,
	SYSTEMS,
	WEIGHTS,
	COSTS;
}