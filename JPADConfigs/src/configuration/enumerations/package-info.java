/**
 * The package contains all the enumerations used throughout the application.
 * 
 * @author Lorenzo Attanasio
 *
 */
package configuration.enumerations;