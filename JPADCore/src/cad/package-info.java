/**
 * Group together all the classes used to generate the CAD model.
 * The project depends on the (C++) OpenCASCADE libraries.
 * 
 * @author Lorenzo Attanasio
 *
 */
package cad;