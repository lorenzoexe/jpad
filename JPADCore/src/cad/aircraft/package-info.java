/**
 * This package contains the classes which generate the CAD model of the main
 * components of the aircraft: the fuselage and the lifting surfaces.
 * 
 * @author Lorenzo Attanasio
 *
 */
package cad.aircraft;