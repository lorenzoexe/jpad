/**
 * This package contains all the database files and the classes needed to
 * properly read the data.
 * 
 * @author Lorenzo Attanasio
 *
 */
package database;