/**
 * The package contains a collection of classes useful to estimate the aerodynamic
 * parameters related to the aircraft and to its flight conditions. 
 * 
 * @author Lorenzo Attanasio
 *
 */
package calculators.aerodynamics;