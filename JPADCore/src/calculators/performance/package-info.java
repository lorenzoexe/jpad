/**
 * This package groups together all the classes needed 
 * to estimate an aircraft performance
 * 
 * @author Lorenzo Attanasio
 *
 */
package calculators.performance;