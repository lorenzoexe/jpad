/**
 * Group together all the classes used to store and retrieve the data
 * related to the evaluation of the performance of the aircraft.
 * 
 * @author Lorenzo Attanasio
 *
 */
package calculators.performance.customdata;