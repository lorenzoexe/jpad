/**
 * This package contains an abstract model of a generic component of the aircraft
 * Each class contains quantities common to all or most of the components.
 * Each class is usually extended by the actual component classes.
 * 
 * All the classes contained in this package are abstract classes. 
 * 
 * @author Lorenzo Attanasio
 *
 */
package aircraft.componentmodel;