/**
 * This package contains the aircraft model and all the classes used to handle
 * the calculation of its parameters (aerodynamics, weights, performances, ...).
 * Each component is defined extending the classes contained in the componentmodel package.
 * 
 * It also contains an OperatingConditions class which defines a reference operating
 * condition (in terms of altitude and mach number) for the aircraft.
 * 
 * 
 * @author Lorenzo Attanasio
 *
 */
package aircraft;