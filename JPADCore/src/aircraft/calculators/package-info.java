/**
 * Group together all classes which contain the data
 * and the methods used for estimating the aircraft parameters.
 * Each class usually contains a method which invokes the ones needed 
 * to estimate all the parameters relative to that class in the proper order. 
 * 
 * NOTICE: Most of the methods just wrap static methods which can be found in
 * the JPADCalculators project.
 * 
 * @author Lorenzo Attanasio
 *
 */
package aircraft.calculators;