/**
 * The package contains all the classes needed to manage the nacelles.
 * The NacellesManager class controls all the existing Nacelle objects and
 * starts the computations (aerodynamics, weights, etc.) for each one.
 * 
 * @author Lorenzo Attanasio
 *
 */
package aircraft.components.nacelles;