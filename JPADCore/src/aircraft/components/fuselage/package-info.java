/**
 * Group all the classes which handle the fuselage component.
 * Fuselage aerodynamics, weights, etc. are estimated and stored in a dedicated class.
 * 
 * @author Lorenzo Attanasio
 *
 */
package aircraft.components.fuselage;