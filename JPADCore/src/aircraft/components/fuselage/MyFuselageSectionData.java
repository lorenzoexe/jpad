package aircraft.components.fuselage;

public enum MyFuselageSectionData {
	
	NOSE_TIP,
	NOSE_CAP,
	MID_NOSE,
	CYLINDRICAL_BODY_1,
	CYLINDRICAL_BODY_2,
	MID_TAIL,
	TAIL_CAP,
	TAIL_TIP

}
