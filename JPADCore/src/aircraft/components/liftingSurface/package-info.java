/**
 * LS stands for Lifting Surface.
 * Each actual lifting surface class (e.g., Wing) is derived from the LiftingSurface class.
 * 
 * @author Lorenzo Attanasio
 *
 */
package aircraft.components.liftingSurface;