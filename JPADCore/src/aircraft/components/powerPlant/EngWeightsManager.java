package aircraft.components.powerPlant;

import static java.lang.Math.round;

import javax.measure.quantity.Force;
import javax.measure.quantity.Mass;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;

import aircraft.componentmodel.InnerCalculator;
import aircraft.components.Aircraft;
import configuration.enumerations.AnalysisTypeEnum;
import configuration.enumerations.EngineTypeEnum;
import configuration.enumerations.MethodEnum;
import standaloneutils.atmosphere.AtmosphereCalc;
import writers.JPADStaticWriteUtils;

public class EngWeightsManager extends aircraft.componentmodel.componentcalcmanager.WeightsManager{

	private Aircraft _theAircraft;
	private Engine _theEngine;
	private EngineTypeEnum _engineType;

	private Amount<Mass> _totalMass,
	_dryMass,
	_dryMassPublicDomain;
	private Amount<Force> _t0;

	public EngWeightsManager(Aircraft aircraft, Engine engine) {
		_theAircraft = aircraft;
		_theEngine = engine;
		
		initializeDependentData();
		initializeInnerCalculators();
	}

	@Override
	public void initializeDependentData() {
		_t0 = _theEngine.get_t0();
		_dryMassPublicDomain = _theEngine.get_dryMassPublicDomain();
		_engineType = _theEngine.get_engineType();
	}
	
	@Override
	public void initializeInnerCalculators() {
		if (_engineType.equals(EngineTypeEnum.TURBOPROP))
			calculator = new Turboprop();
		else if(_engineType.equals(EngineTypeEnum.TURBOFAN))
			calculator = new Turbofan();
		else if(_engineType.equals(EngineTypeEnum.PISTON))
			calculator = new Piston();		
	}
	
	/** 
	 * Evaluate a single engine (dry) mass
	 * 
	 * @author Lorenzo Attanasio
	 * @param method
	 */
	@Override
	public void calculateAll() {
		calculator.allMethods();

		_methodsMap.put(AnalysisTypeEnum.WEIGHTS, _methodsList);
		_percentDifference =  new Double[_massMap.size()]; 

		_dryMass = Amount.valueOf(JPADStaticWriteUtils.compareMethods(
				_dryMassPublicDomain, 
				_massMap,
				_percentDifference,
				20.).getFilteredMean(), SI.KILOGRAM);

		calculateTotalMass();

	}

	public class Turbofan extends InnerCalculator{

		//page 434 Stanford
		public Amount<Mass> harris() {
			if (_t0.to(NonSI.POUND_FORCE).getEstimatedValue() < 10000) {
				_dryMass = Amount.valueOf(
						Math.pow(0.4054 * _t0.to(NonSI.POUND_FORCE).getEstimatedValue(),0.9255), 
						NonSI.POUND).to(SI.KILOGRAM);

			} else {
				_dryMass = Amount.valueOf(
						Math.pow(0.616 * _t0.to(NonSI.POUND_FORCE).getEstimatedValue(),0.886), 
						NonSI.POUND).to(SI.KILOGRAM);
			}

			_methodsList.add(MethodEnum.HARRIS);
			_massMap.put(MethodEnum.HARRIS, Amount.valueOf(round(_dryMass.getEstimatedValue()), SI.KILOGRAM));
			return _dryMass;
		}

		@Override
		public void allMethods() {
			harris();			
		} 
	}


	public class Turboprop extends InnerCalculator{

		// page 434 Stanford		
		public Amount<Mass> harris() {
			if (_t0.to(NonSI.POUND_FORCE).getEstimatedValue() < 10000) {
				_dryMass = Amount.valueOf(
						Math.pow(0.4054 * _t0.to(NonSI.POUND_FORCE).getEstimatedValue(),0.9255), 
						NonSI.POUND).to(SI.KILOGRAM);

			} else {
				_dryMass = Amount.valueOf(
						Math.pow(0.616 * _t0.to(NonSI.POUND_FORCE).getEstimatedValue(),0.886), 
						NonSI.POUND).to(SI.KILOGRAM);
			}
			_methodsList.add(MethodEnum.HARRIS);
			_massMap.put(MethodEnum.HARRIS, Amount.valueOf(round(_dryMass.getEstimatedValue()), SI.KILOGRAM));
			return _dryMass;
		}

		@Override
		public void allMethods() {
			harris();			
		}
	}


	public class Piston extends InnerCalculator {

		@Override
		public void allMethods() {
			// TODO Auto-generated method stub
		}

	}

	/**
	 * Calculate engine mass only if public domain data
	 * about engine is not available
	 */
	public void calculateTotalMass() {

		if (_dryMassPublicDomain != null) {
			_dryMass = Amount.valueOf(_dryMassPublicDomain.getEstimatedValue(), SI.KILOGRAM);
		}

		// TORENBEEK_1982 method gives better results for 50000 < MTOM < 200000
		if (_engineType.equals(EngineTypeEnum.TURBOPROP) | ( 
				_theAircraft.get_weights().get_MTOM().getEstimatedValue() < 50000 |
				_theAircraft.get_weights().get_MTOM().getEstimatedValue() > 200000)) {
			calculateTotalMass(MethodEnum.TORENBEEK_1982);

		} else {
			calculateTotalMass(MethodEnum.TORENBEEK_2013);
		}

	}

	/**
	 * Calculate total engine mass
	 * 
	 * @author Lorenzo Attanasio
	 * @param method
	 * @return 
	 */
	public Amount<Mass> calculateTotalMass(MethodEnum method) {

		switch(method){

		case TORENBEEK_1982 : {
			_totalMass = _dryMass.times(1.377);
		} break;

		case TORENBEEK_2013 : {
			_totalMass = Amount.valueOf((
					_t0.times(0.25).getEstimatedValue() + 
					8000)/AtmosphereCalc.g0.getEstimatedValue(), SI.KILOGRAM);
		} break;

		default : break;
		}

		return _totalMass;
	}

	public Amount<Mass> get_dryMassPublicDomain() {
		return _dryMassPublicDomain;
	}

	public void set_dryMassPublicDomain(Amount<Mass> _dryMassPublicDomain) {
		this._dryMassPublicDomain = _dryMassPublicDomain;
	}

	public Amount<Mass> get_totalMass() {
		return _totalMass;
	}


}
