/**
 * The package manages all the classes related to the Power plant.
 * The PowerPlant class controls all the existing Engine objects and
 * starts the computations (aerodynamics, weights, etc.) for each one.
 * 
 * @author Lorenzo Attanasio
 *
 */
package aircraft.components.powerPlant;