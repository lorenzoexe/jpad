/**
 * Contains the classes that manage the airfoil geometry and aerodynamics properties and methods.
 * These are grouped in specific classes; an object for each class is then created in the main
 * class which manage the the airfoil entity.
 * 
 * @author Lorenzo Attanasio
 *
 */
package aircraft.auxiliary.airfoil;