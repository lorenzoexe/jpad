/**
 * Contains all the classes necessary to handle specific aspects of the aircraft component;
 * e.g., the airfoil related classes.
 * 
 * @author Lorenzo Attanasio
 *
 */
package aircraft.auxiliary;